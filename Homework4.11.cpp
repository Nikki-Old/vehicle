﻿#include <iostream>
#include <vector>

class Vehicle
{
public:

	virtual std::ostream& print (std::ostream& out) const
	{
		return out;
	}

	virtual float getPower () const
	{
		return 0.f;
	}

	friend std::ostream& operator<<(std::ostream& out, const Vehicle& obj)
	{
		return obj.print (out);
	}

};

class WaterVehicle : public Vehicle
{
public:
	WaterVehicle (float seaGauge)
	{
		this->SeaGauge = seaGauge;
	}

	virtual std::ostream& print (std::ostream& out) const
	{
		out << "Water vehicle: " << SeaGauge << std::endl;
		return out;
	}

	float SeaGauge/*осадкаСудна*/ = 0.f;
};

class RoadVehicle : public Vehicle
{
public:

	float GroundClearance/*дорожныйПросвет*/ = 0.f;

	int CounterWheels = 0;
};

class Wheel
{
public:
	Wheel () { std::cout << "Spawn Wheel" << std::endl; }

	Wheel (float Diameter)
	{
		this->Diameter = Diameter;
	}

	Wheel& operator=(const Wheel& other)
	{

		this->Diameter = other.Diameter;

		return *this;
	}

	friend std::ostream& operator<<(std::ostream& out, const Wheel& obj)
	{
		return out << " " << obj.Diameter << " ";
	}

	float Diameter = 0.f;
};

class Engine
{
public:
	Engine () { std::cout << "Spawn Engine" << std::endl; }

	Engine (float Power)
	{
		this->Power = Power;
	}

	Engine& operator=(const Engine& other)
	{
		this->Power = other.Power;

		return *this;
	}

	friend std::ostream& operator<<(std::ostream& out, const Engine& obj)
	{
		out << " " << obj.Power << " ";
		return out;
	}

	float Power = 0.f;
};

class Bicycle : public RoadVehicle
{
public:

	Bicycle (Wheel w1, Wheel w2, float GroundClearance)
	{
		Wheels = new Wheel * [2];
		Wheels[CounterWheels++] = new Wheel (w1);
		Wheels[CounterWheels++] = new Wheel (w2);

		this->GroundClearance = GroundClearance;
	}

	std::ostream& print (std::ostream& out) const override
	{
		out << "Bicycle " << "Wheel:";
		for (int i = 0; i < CounterWheels; i++)
			out << *Wheels[i];

		out << "Ride height: " << GroundClearance;

		return out;
	}

	Wheel** Wheels = nullptr;

};

class Car : public RoadVehicle
{
public:
	Car (Engine engine, Wheel w1, Wheel w2, Wheel w3, Wheel w4, float GroundClearance)
	{
		CarEngine = new Engine (engine);

		Wheels = new Wheel * [4];
		Wheels[CounterWheels++] = new Wheel (w1);
		Wheels[CounterWheels++] = new Wheel (w2);
		Wheels[CounterWheels++] = new Wheel (w3);
		Wheels[CounterWheels++] = new Wheel (w4);

		this->GroundClearance = GroundClearance;
	}

	~Car ()
	{
		std::cout << "Call Destructor";
		delete  CarEngine;
		delete[] Wheels;
	}

	std::ostream& print (std::ostream& out) const override
	{
		out << "Car " << "Engine: " << *CarEngine << "Wheels:";

		for (int i = 0; i < CounterWheels; i++)
			out << *Wheels[i];

		out << "Ride height: " << GroundClearance;

		return out;
	}

	float getPower () const override
	{
		return CarEngine->Power;
	}

	Wheel** Wheels = nullptr;

	Engine* CarEngine = nullptr;
};

class Point
{
public:
	Point (int x, int y, int z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	friend std::ostream& operator<<(std::ostream& out, const Point& obj)
	{
		out << "Point: x = " << obj.x << " y = " << obj.y << " z = " << obj.z;
		return out;
	}

	int x = 0;
	int y = 0;
	int z = 0;
};
class Circle : public Vehicle
{
public:

	Circle (Point point, float radius)
	{
		this->point = new Point (point);
		this->Radius = radius;
	}

	std::ostream& print (std::ostream& out) const override
	{
		out << "Circle: " << *point << "Radius: " << Radius;
		return out;
	}

	Point* point;
	float Radius;
};

float getHighestPower (std::vector<Vehicle*> v)
{
	float MaxPower = 0.0f;

	for (int i = 0; i < v.size (); i++)
	{
		if (MaxPower < v.at (i)->getPower ())
		{
			MaxPower = v.at (i)->getPower ();
		}
	}

	return MaxPower;
}

int main ()
{

	/*std::cout << " Good " << std::endl;

	Car c (Engine (150), Wheel (17), Wheel (17), Wheel (18), Wheel (18), 150);

	std::cout << c << '\n';

	Bicycle t (Wheel (20), Wheel (20), 300);

	std::cout << t << '\n';*/

	std::vector<Vehicle*> v;

	v.push_back (new Car (Engine (150), Wheel (17), Wheel (17), Wheel (18), Wheel (18), 250));

	v.push_back (new Circle (Point (1, 2, 3), 7));

	v.push_back (new Car (Engine (200), Wheel (19), Wheel (19), Wheel (19), Wheel (19), 130));

	v.push_back (new WaterVehicle (5000));



	//TODO: Вывод элементов вектора v здесь
	for (int i = 0; i < v.size (); i++)
	{
		std::cout << *v.at (i) << std::endl;
	}


	std::cout << "The highest power is " << getHighestPower (v) << '\n'; // реализуйте эту функцию

	//TODO: Удаление элементов вектора v здесь

	for (int i = 0; i < v.size (); i++)
	{
		delete v.at (i);
	}
}